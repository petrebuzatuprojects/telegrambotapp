<?php
namespace AppBundle\Services;




use AppBundle\Entity\Ticket;
use Symfony\Bridge\Doctrine\ManagerRegistry;

class TelegramBot
{

    /** @var  ManagerRegistry */
    private $manager;

    public function sendRequest($ticketId, $message)
    {

        $ticket= $this->getManager()->getRepository(Ticket::class)->find($ticketId);
        $ticket->setReply($message);
        $ticket->setIsAnswered(true);

        $this->getManager()->persist($ticket);
        $this->getManager()->flush();
        $mesID = $ticket->getMessageId();

        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://api.telegram.org/bot1036412052:AAG4Kq3gYbNByldvOj6ozk3V218HOanVrXE/sendMessage?chat_id=-1001498595065&text=$message&reply_to_message_id=$mesID",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_HTTPHEADER => array(
                "Accept: */*",
                "Accept-Encoding: gzip, deflate",
                "Cache-Control: no-cache",
                "Connection: keep-alive",
                "Content-Length: 0",
                "Host: api.telegram.org",
                "Postman-Token: 7a233de7-66d6-4d69-aeff-20d2e7022072,42fcc18e-084d-4b71-8784-ca3bb927c7fb",
                "User-Agent: PostmanRuntime/7.20.1",
                "cache-control: no-cache"
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            echo "cURL Error #:" . $err;
        } else {
            echo $response;
        }
    }


    public function getUpdates(){

        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://api.telegram.org/bot1036412052:AAG4Kq3gYbNByldvOj6ozk3V218HOanVrXE/getUpdates?chat_id =-1001498595065",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
                "Accept: */*",
                "Accept-Encoding: gzip, deflate",
                "Cache-Control: no-cache",
                "Connection: keep-alive",
                "Host: api.telegram.org",
                "Postman-Token: fd6168e1-d22d-495b-a32b-0baf2c5d21a3,39a0a798-ccb4-4fa0-ad8c-e47b159cb177",
                "User-Agent: PostmanRuntime/7.20.1",
                "cache-control: no-cache"
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            echo "cURL Error #:" . $err;
        }

        $ticket= new Ticket();
        $mes=json_decode($response);
        $res=$mes->result;

        $arrAdmin = $this->getAdmin();
        $mesAdmin = json_decode($arrAdmin);

        //find admin users
        $resAdmin = $mesAdmin -> result;
        $adminID = []; $i = 0;
        foreach ($resAdmin as $admin){
            $adminID[$i]= $admin ->user->id;
            $i++;
        }



        $lastMes = $this->getManager()->getRepository(Ticket::class)->findOneBy([], ['id' => 'desc']);

        //if empty db so it doesnt show error for null id
        if($lastMes==null) {
            $messId= -1;
        }
        else{
            $messId = $lastMes->getMessageId();
        }



        foreach ($res as $line){
            $sameID = false;
            foreach ($adminID as $userID){
                if ($userID == $line->message->from->id){
                    $sameID = true;
                    break;
                }
            }
            //to insert newer messages than the ones already in db
            if ($line->message->message_id > $messId&&(!$sameID)) {
                $ticket->setName($line->message->from->first_name);
                $ticket->setMessage($line->message->text);
                $data = date('d-m-Y', $line->message->date);
                $mesdata = \DateTime::createFromFormat('d-m-Y', $data);
                $ticket->setDate($mesdata);
                $ticket->setMessageId($line->message->message_id);

                $this->getManager()->persist($ticket);

                $this->getManager()->flush();

            }

            $ticket = new Ticket();
        }


    }

    public function getAdmin()

    {
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://api.telegram.org/bot1036412052:AAG4Kq3gYbNByldvOj6ozk3V218HOanVrXE/getChatAdministrators?chat_id=-1001498595065",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
                "Accept: */*",
                "Accept-Encoding: gzip, deflate",
                "Cache-Control: no-cache",
                "Connection: keep-alive",
                "Host: api.telegram.org",
                "Postman-Token: 48d01b69-64a6-4dcb-8330-b606eefd74b7,34e2c98c-e976-4de3-a9ee-920cead0d199",
                "User-Agent: PostmanRuntime/7.20.1",
                "cache-control: no-cache"
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            echo "cURL Error #:" . $err;
        } else {
            return $response;
        }

    }

    /**
     * @return ManagerRegistry
     */
    public function getManager()
    {
        return $this->manager;
    }

    /**
     * @param ManagerRegistry $manager
     * @return TelegramBot
     */
    public function setManager($manager)
    {
        $this->manager = $manager;
        return $this;
    }



}


