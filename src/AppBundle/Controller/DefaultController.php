<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use AppBundle\Entity\Ticket;



class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction()
    {
        return $this->render('default/index.html.twig');
    }


    /**
     * @Route("update", name="update")
     */
    public function updateAction(){
        $this->get('telegrambot.service')->getUpdates();

        return $this->redirectToRoute('list');
    }


    /**
     * @Route("list", name="list")
     */
    public function listAction(Request $request)
    {

        $em= $this->getDoctrine()->getManager();
        $messages= $em->getRepository(Ticket::class)->findBy(['isAnswered'=>false]);
        return $this->render('default/list.html.twig',['messages'=>$messages]);

    }


    /**
     * @Route("reply", name="reply")
     */
    public function replyAction(Request $request)
    {
        $data=$request->request->all();

        $this->get('telegrambot.service')->sendRequest($data["id"], $data['reply']);


        return $this->redirectToRoute('list');


    }



    }
